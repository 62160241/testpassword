const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Lenght', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true);
  });

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false);
  });

  test('should 25 characters to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true);
  });

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false);
  });
})

describe('Test Alphabet', () => {
  test('should has alphabet a in password', () => {
    expect(checkAlphabet('a')).toBe(true)
  })
  test('should has alphabet B in password', () => {
    expect(checkAlphabet('B')).toBe(true)
  })
  test('should has alphabet C in password', () => {
    expect(checkAlphabet('C')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1234')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has ditgit to be true', () => {
    expect(checkDigit('A1234323')).toBe(true)
  })
  test('should has not ditgit in password', () => {
    expect(checkDigit('abc')).toBe(false)
  })
  test('should has not ditgit in password', () => {
    expect(checkDigit('abcdefg.')).toBe(false)
  })
  test('should 7 charecter to be false', () => {
    expect(checkDigit('123456%')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol in password to be true', () => {
    expect(checkSymbol('1234!')).toBe(true)
  })
  test('should has number in password to be false', () => {
    expect(checkSymbol('1234')).toBe(false)
  })
  test('should has not have  symbol in password to be false', () => {
    expect(checkSymbol('1234a')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password NitroEz123 to be false', () => {
    expect(checkPassword('NitroEz123')).toBe(false)
  })
  test('should password NitroEz@123 to be true', () => {
    expect(checkPassword('NitroEz@123')).toBe(true)
  })
})

